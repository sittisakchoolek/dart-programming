// Name: Sittisak Choolak
// Student ID: 6450110013

main() {
  for (int i=1; i<=9; i++) {
    print("\nMultiplication table of $i:");
    for (int j=1; j<=12; j++) {
      print("$i X $j = ${i*j}");
    }
  }
}