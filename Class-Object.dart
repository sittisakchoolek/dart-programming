// Name: Sittisak Choolak
// Student ID: 6450110013

class Laptop {
  show() {
    print("Laptop display.");
  }
}

class MacBook extends Laptop {
  @override
  show() {
    print("MacBook display.");
    super.show();
  }
}

class MacBookPro extends MacBook {
  @override
  show() {
    print("MacBookPro display.");
    super.show();
  }
}

main() {
  MacBookPro macbookpro = new MacBookPro();
  macbookpro.show();
}